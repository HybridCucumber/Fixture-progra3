
public class FormarFixture {

    public FormarFixture(int[][] t, int inf, int sup) {


        if (inf == sup - 1) {
            t[inf][0] = sup;
            t[sup][0] = inf;
        } else {
            int medio = (inf+1 + sup) / 2;
            new FormarFixture(t, inf, medio);
            new FormarFixture(t, medio + 1, sup);
            completarTabla(t, inf, medio, medio, sup - 1, medio + 1);
            completarTabla(t, medio + 1, sup, medio, sup - 1, inf);
        }

    }






    private void completarTabla(int[][] t, int equipoInferior, int equipoSuperior,
                                int diaInferior, int diaSuperior,int equipoInicial){
    int j = diaInferior;
    while(j<= diaSuperior){
        t[equipoInferior][j] = equipoInicial + j - diaInferior;
    }
    int i = equipoInferior+1;
    while(i<=equipoSuperior){
        t[i][diaInferior] = t[i-1][diaSuperior];
        j=diaInferior+1;
        while(j<=diaSuperior){
            t[i][j] = t[i-1][j-1];
        }
        }
    }






}